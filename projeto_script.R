### Carregando os pacotes ###----

pacman::p_load(read.csv, tidyverse, ggplot2, readxl,dplyr,lubridate, nortest,scales)
library(pacman)

### Carregando os dados ###----
setwd("C:/Users/patym/OneDrive/Área de Trabalho/ESTAT")
dados = read.csv2("DunderMifflinCompany.csv", sep=";")

### Dados paises ###
PAIS = read_excel("paises - Copia.xlsx", sheet = NULL)

### Dados final para obtencao da analise ###
DADOS <- merge(dados,PAIS, by = c("City", "City"), all = TRUE)


### Renomeando as colunas ###----

colnames(DADOS) <- c('Cidade','ID','ID Empregado','Nome','Cargo','Departamento','Negocio','Genero','Etnia','Idade','Data','Salario','Bonus','Saida','País')

DADOS <- DADOS %>%
  mutate(País = recode(País, "Brazil" = "Brasil", "United States" = "Estados Unidos"))

DADOS <- DADOS %>%
  mutate (Negocio = recode(Negocio, "Manufacturing" = "Manufatura", "Corporate" = "Corporativo","Specialty Products" = "Produtos\nespeciais","Research & Development" = "Pesquisa\n e desenvolvimento"))   # renomeando valores dentro da coluna do data frame usando a função recode()

# Remover valores duplicados # 

DADOS <- DADOS %>% distinct(`ID Empregado`, .keep_all = TRUE)

### Padronizacao estat ###---
cores_estat <- c("#A11D21", "#003366", "#CC9900", "#663333", "#FF6600", "#CC9966", "#999966", "#006606", "#008091", "#041835", "#666666")

theme_estat <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 12),
      axis.title.x = ggplot2::element_text(colour = "black", size = 12),
      axis.text = ggplot2::element_text(colour = "black", size = 9.5),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top",
      ...
    )
  
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}

theme_estat2 <- function(...) {
  theme <- ggplot2::theme_bw() +
    ggplot2::theme(
      axis.title.y = ggplot2::element_text(colour = "black", size = 10),
      axis.title.x = ggplot2::element_text(colour = "black", size = 10),
      axis.text = ggplot2::element_text(colour = "black", size = 10),
      panel.border = ggplot2::element_blank(),
      axis.line = ggplot2::element_line(colour = "black"),
      legend.position = "top", 
      legend.justification = "left",
      legend.margin = margin(l = -10, unit = "pt"),
      ...
    )
  return(
    list(
      theme,
      scale_fill_manual(values = cores_estat),
      scale_colour_manual(values = cores_estat)
    )
  )
}


### 1) Número de contratações ao longo dos anos por país de origem dos funcionários;###----

# Formatação da data #
DADOS$Ano <- as.POSIXct(DADOS$Data, format = "%m/%d/%Y ")
DADOS$Ano <- format(DADOS$Ano, format="%Y")

DADOS$Ano <- as.numeric(DADOS$Ano)

# A contagem do numero de contrações por páis #
numero <- DADOS %>% 
          group_by(País,Ano) %>%
          summarise(quantidade = n()) 


# Formatação do paises #
numero <- numero %>%
  mutate(País = recode(País, "Brazil" = "Brasil", "United States" = "Estados Unidos"))


# Gerar o gráfico #
ggplot(numero) +
  aes(x = Ano, y = quantidade, group = País, colour = País) +
  geom_line(size = 1) +
  geom_point(size = 2) +
  scale_colour_manual(name = "País", labels = c("Brasil", "China", "Estados Unidos")) +
  labs(x = "Ano", y = "Nº de contratações") +
  scale_x_continuous(name = "Ano",
    breaks = (seq(1990,2022,2)),
    labels = as.character(seq(1990, 2022,2))) +
  theme_estat()
ggsave("series_paises.pdf", width = 158, height = 93, units = "mm")




### 2)Salário anual por unidade de negócio;###----

# Formatacao da variavel salario #
DADOS$Salario <- gsub("\\$", "", DADOS$Salario) # removendo o caracter especial $
DADOS$Salario <- gsub(",", "", DADOS$Salario) # substituindo a vírgula pelo o ponto
DADOS$Salario <- as.numeric(DADOS$Salario)


# Manipulacao #
salario_por_unidade <- select(DADOS, Negocio,Salario)

salario_por_unidade <- mutate(salario_por_unidade, Negocio = recode(Negocio, "Manufacturing" = "Manufatura", "Corporate" = "Corporativo","Specialty Products" = "Produtos\nespeciais","Research & Development" = "Pesquisa\n e desenvolvimento"))   # renomeando valores dentro da coluna do data frame usando a função recode()

salarios_anual <- as_tibble(salario_por_unidade) # transformando o data frame em um tibble

# analisando o salário anual por unidade de negócios
salario_anual <- salarios_anual %>%
                 group_by(Negocio) %>%
                 summarise(media_salario = mean(Salario))

# medidas resumo #

medidas_resumo <- aggregate(Salario ~ Negocio, data = salario_por_unidade, 
                            FUN = function(x) c(media = mean(x),
                                                desvio = sd(x),
                                                mediana = median(x), 
                                                minimo = min(x), 
                                                maximo = max(x),
                                                quartil = quantile(x)))



# Grafico BOXPLOT 

salario_por_unidade %>%
  ggplot(aes(x = Negocio, y = Salario)) +
  geom_boxplot(fill = c("#A11D21"), width = 0.5) +
  stat_summary(
    fun = "mean", geom = "point", shape = 23, size = 3, fill = "white"
  ) +
  labs(x = "Negócios", y = " Salários(R$) ") +
  
  theme_estat(axis.text.x = element_text(angle = 0, vjust = 0.8, size = ))
ggsave("box_salario1.pdf", width = 158, height = 93, units = "mm")


### TESTES ###

options(scipen = 999) # formatação das unidades 

# Lilliefors / Kolmogorov #
by(salario_por_unidade$Salario, salario_por_unidade$Negocio,FUN = lillie.test )
by(salario_por_unidade$Salario, salario_por_unidade$Negocio, FUN = function(x) ks.test(x, "pnorm", mean(x), sd(x)))

# Shapiro #

shapiro.test(salario_por_unidade$Salario)

# Kruskal-Wallis #

kruskal.test(Salario ~ Negocio, data = salario_por_unidade)

### 03) Unidade de negocio por pais ###---

negocios_paises <- select(DADOS, Negocio,País)

negocios_paises <- mutate(negocios_paises, Negocio = recode(Negocio, "Pesquisa\n e desenvolvimento" = "Pesquisa", "Produtos\nespeciais"= "Produtos"))   # renomeando valores dentro da coluna do data frame usando a função recode().

#Agrupando os dados por país e contando o número de unidades de negócios em cada país

negocios<- negocios_paises %>%
  group_by(País) %>%
  count(Negocio) %>%
  mutate(freq_relativa = percent(n / sum(n)))%>%
  mutate(
    label = str_c(n, " (", freq_relativa, ")") %>% str_squish()
  )



# Gráfico #
ggplot(negocios) +
  aes(
    x = fct_reorder(País, n, .desc = F), y = n,
    fill = Negocio, label = label,
    x = fct_reorder(País, freq, .desc = T), y = freq_relativa,
    fill = Negocio, label = legendas
  ) +
  geom_col(position = position_dodge2(preserve = "single", padding = 0)) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = 0.5, hjust = -0.1,
    size = 3
  ) +
  ylim(0,250)+
  labs(x = "Países", y = "Número de contratações") +
  theme_estat2()+
  coord_flip()
ggsave("pais-negocio.pdf", width = 158, height = 93, units = "mm")



### 04) Relação entre a felicidade dos funcionários e a satisfação com o trabalho; ###---

FELICIDADE = read.csv2("Felicidade.csv", sep=",")

# transformando em numérico
FELICIDADE$Felicidade <- as.numeric(FELICIDADE$Felicidade)

FELICIDADE$Satisfação.com.o.trabalho <- as.numeric(FELICIDADE$Satisfação.com.o.trabalho)

# Grafico #
ggplot(FELICIDADE) +
  aes(x = Felicidade, y = Satisfação.com.o.trabalho) +
  geom_point(colour = "#A11D21", size = 3) +
  labs(
    x = "Felicidade dos funcionários",
    y = "Satisfação com o trabalho" )+ 
  theme_estat()
ggsave("disp_FEL.pdf", width = 158, height = 93, units = "mm")

# Teste de Normalidade #
#  Shapiro #
shapiro.test(FELICIDADE$Felicidade)
shapiro.test(FELICIDADE$Satisfação.com.o.trabalho)

# Correlação #
cor.test(FELICIDADE$Felicidade, FELICIDADE$Satisfação.com.o.trabalho, method = "pearson")




### 05) Número de funcionários por departamento; ###---

departamentos <- DADOS %>%
  filter(!is.na(Departamento)) %>%
  count(Departamento) %>%
  mutate(
    freq = n / sum(n),
    freq = percent(freq, accuracy = 0.1)
  ) %>%
  mutate(
    label = str_c(n, " (", freq, ")") %>% str_squish()
  )



# Formatação das unidades de Negócio #
departamentos <- departamentos %>%
  mutate(Departamento = recode(Departamento, "Sales" = "Venda", "Finance" = "Finanças", "Engineering" = "Engenharia", 
                               "Marketing" = "Marketing", "Human Resources"= "Recursos\nHumanos",
                               "Accounting" = "Contabilidade", "IT" = "Tecnologia"))





## Grafico ##

ggplot(departamentos) +
  aes(x = fct_reorder(Departamento, n, .desc=F), y = n, label = label) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, #hjust = .5,
    size = 3
  ) + 
  labs(x = "Departamento", y = "Número de funcionários") +
  theme_estat() +
  coord_flip()
ggsave("colunas-departamento.pdf", width = 158, height = 93, units = "mm")















>>>>>>> f3cf2d792fc2574a23025796192be72b2a19f98b




<<<<<<< HEAD
### 05) Número de funcionários por departamento; ###---

departamentos <- DADOS %>%
  filter(!is.na(Departamento)) %>%
  count(Departamento) %>%
  mutate(
    freq = n / sum(n),
    freq = percent(freq, accuracy = 0.1,decimal.mark = ",")
  ) %>%
  mutate(
    label = str_c(n, " (", freq, ")") %>% str_squish()
  )



# Formatação das unidades de Negócio #
departamentos <- departamentos %>%
  mutate(Departamento = recode(Departamento, "Sales" = "Venda", "Finance" = "Finanças", "Engineering" = "Engenharia", 
                               "Marketing" = "Marketing", "Human Resources"= "Recursos\nHumanos",
                               "Accounting" = "Contabilidade", "IT" = "Tecnologia"))





## Grafico ##

ggplot(departamentos) +
  aes(x = fct_reorder(Departamento, n, .desc=F), y = n, label = label) +
  geom_bar(stat = "identity", fill = "#A11D21", width = 0.7) +
  geom_text(
    position = position_dodge(width = .9),
    vjust = -0.5, hjust = -0.1,
    size = 3
  ) + 
  ylim(0,350)+
  labs(x = "Departamento", y = "Número de funcionários") +
  theme_estat()+ 
  coord_flip()
ggsave("departamento-funcionarios.pdf", width = 158, height = 93, units = "mm")

=======
>>>>>>> f3cf2d792fc2574a23025796192be72b2a19f98b




